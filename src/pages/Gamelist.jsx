import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import { Link } from 'react-router-dom';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Links from '@mui/material/Link';
import GlobalStyles from '@mui/material/GlobalStyles';

function Gamelist() {
  return (
    <React.Fragment>
      <GlobalStyles styles={{ ul: { margin: 0, padding: 0, listStyle: 'none' } }} />
      <CssBaseline />
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}
      >
        <Toolbar sx={{ flexWrap: 'wrap' }}>
          <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
            Fun Portal
          </Typography>
          <nav>
            <Links
              variant="button"
              color="text.primary"
              href="/game"
              sx={{ my: 1, mx: 1.5 }}
            >
              Game
            </Links>
            <Links
              variant="button"
              color="text.primary"
              href="#"
              sx={{ my: 1, mx: 1.5 }}
            >
              Account
            </Links>
          </nav>
          <Button variant="outlined" sx={{ my: 1, mx: 1.5 }}>
            <Link to="/login">Logout</Link>
          </Button>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}

export default Gamelist;