import logo from './logo.svg';
import './App.css';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';
import Layout from './pages/Layout';
import NotFound from './pages/NotFound';
import Login from './pages/Login';
import Register from './pages/Register';
import * as React from "react";
import { Routes, Route, Outlet, Link } from "react-router-dom";

function App() {
  return (
    <div className="container">
    <div>
      <Routes>
        <Route path="/" element={<Layout />}>

          <Route index element={<Home />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />

        </Route>
        <Route path="/no-layout" element={<Contact />} />
        {/* handle 404 */}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
    </div>
  );
}

export default App;
